#include <iostream>
#include <map>
using namespace std;
map<long long, long long> mp;
long long go(long long u, long long v, int w)
{
	long long ans = 0;
	while (u != v)
		if (u > v)
		{
			ans += mp[u];
			mp[u] += w;
			u /= 2;
		}
		else
		{
			ans += mp[v];
			mp[v] += w;
			v /= 2;
		}
	return ans;
}
int main()
{
	int q;
	cin >> q;
	while (q--)
	{
		int t;
		cin >> t;
		if (t == 1)
		{
			long long u, v;
			int w;
			cin >> u >> v >> w;
			go(u, v, w);
		}
		else
		{
			long long u, v;
			cin >> u >> v;
			cout << go(u, v, 0) << endl;
		}
	}
	return 0;
}
