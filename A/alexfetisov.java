import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.io.BufferedReader;
import java.util.Collections;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 *
 * @author AlexFetisov
 */
public class Main {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        TaskA1_362 solver = new TaskA1_362();
        solver.solve(1, in, out);
        out.close();
    }

    static class TaskA1_362 {
        HashMap<Long, Long> dict;

        public void solve(int testNumber, InputReader in, PrintWriter out) {
            int q = in.nextInt();
            dict = new HashMap<Long, Long>();
            for (int i = 0; i < q; ++i) {
                int type = in.nextInt();
                if (type == 1) {
                    long v = in.nextLong();
                    long u = in.nextLong();
                    long lca = getLCA(v, u);
                    int w = in.nextInt();
                    while (v != lca) {
                        addToDict(v, w);
                        v /= 2;
                    }
                    while (u != lca) {
                        addToDict(u, w);
                        u /= 2;
                    }
                } else {
                    long v = in.nextLong();
                    long u = in.nextLong();
                    long lca = getLCA(v, u);
                    long sum = 0;
                    while (v != lca) {
                        if (dict.containsKey(v)) {
                            sum += dict.get(v);
                        }
                        v /= 2;
                    }
                    while (u != lca) {
                        if (dict.containsKey(u)) {
                            sum += dict.get(u);
                        }
                        u /= 2;
                    }
                    out.println(sum);
                }
            }
        }

        long getLCA(long v, long u) {
            List<Long> a = new ArrayList<Long>();
            List<Long> b = new ArrayList<Long>();
            while (v > 0) {
                a.add(v);
                v /= 2;
            }
            while (u > 0) {
                b.add(u);
                u /= 2;
            }
            Collections.reverse(a);
            Collections.reverse(b);
            long res = -1;
            for (int i = 0; i < a.size() && i < b.size(); ++i) {
                if (a.get(i).equals(b.get(i))) {
                    res = a.get(i);
                }
            }
            return res;
        }

        void addToDict(long v, int w) {
            if (dict.containsKey(v)) {
                dict.put(v, dict.get(v) + w);
            } else {
                dict.put(v, (long) w);
            }
        }

    }

    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer stt;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                return null;
            }
        }

        public String nextString() {
            while (stt == null || !stt.hasMoreTokens()) {
                stt = new StringTokenizer(nextLine());
            }
            return stt.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(nextString());
        }

        public long nextLong() {
            return Long.parseLong(nextString());
        }

    }
}

