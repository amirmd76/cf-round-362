a = raw_input().split(' ')
t = int(a[0])
s = int(a[1])
x = int(a[2])
if x == t:
	print("YES")
elif x < s + t:
	print("NO")
else:
	r = (x - t) % s
	if r <= 1:
		print("YES")
	else:
		print("NO")
