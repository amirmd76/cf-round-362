import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.math.BigInteger;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 *
 * @author AlexFetisov
 */
public class Main {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        TaskC_362 solver = new TaskC_362();
        solver.solve(1, in, out);
        out.close();
    }

    static class TaskC_362 {
        public void solve(int testNumber, InputReader in, PrintWriter out) {
            // Formula: x / y
            // x = 2^(n-1) +- 1^(n-1) / 3
            // y = 2^(n-1)
            int k = in.nextInt();
            final int MOD = (int) 1e9 + 7;
            final int _2 = BigInteger.valueOf(2).modInverse(BigInteger.valueOf(MOD)).intValue();
            final int _3 = BigInteger.valueOf(3).modInverse(BigInteger.valueOf(MOD)).intValue();
            long p2 = 2;
            boolean all1 = true;
            boolean odd = true;
            while (k-- > 0) {
                long a = in.nextLong();
                if (a != 1) {
                    all1 = false;
                }
                p2 = IntegerUtils.pow(p2, a, MOD);
                if (a % 2 == 0) {
                    odd = false;
                }
            }
            if (all1) {
                out.println("0/1");
                return;
            }
            odd = !odd;
            p2 = (p2 * _2) % MOD;
            long x = p2;
            if (odd) {
                x++;
            } else {
                x--;
            }
            x += MOD;
            x %= MOD;
            x = (x * _3) % MOD;
            out.println(x + "/" + p2);
        }

    }

    static class IntegerUtils {
        public static long pow(long x, long p, long MOD) {
            long result = 1;
            for (long i = p; i > 0; i >>= 1) {
                if ((i & 1) != 0) {
                    result = (result * x) % MOD;
                }
                x = (x * x) % MOD;
            }
            return result;
        }

    }

    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer stt;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                return null;
            }
        }

        public String nextString() {
            while (stt == null || !stt.hasMoreTokens()) {
                stt = new StringTokenizer(nextLine());
            }
            return stt.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(nextString());
        }

        public long nextLong() {
            return Long.parseLong(nextString());
        }

    }
}

