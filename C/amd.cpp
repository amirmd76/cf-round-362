#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define Foreach(i, c) for(__typeof((c).begin()) i = (c).begin(); i != (c).end(); ++i)
#define For(i,a,b) for(int (i)=(a);(i) < (b); ++(i))
#define rof(i,a,b) for(int (i)=(a);(i) > (b); --(i))
#define rep(i, c) for(auto &(i) : (c))
#define x first
#define y second
#define pb push_back
#define PB pop_back()
#define iOS ios_base::sync_with_stdio(false)
#define sqr(a) (((a) * (a)))
#define all(a) a.begin() , a.end()
#define error(x) cerr << #x << " = " << (x) <<endl
#define Error(a,b) cerr<<"( "<<#a<<" , "<<#b<<" ) = ( "<<(a)<<" , "<<(b)<<" )\n";
#define errop(a) cerr<<#a<<" = ( "<<((a).x)<<" , "<<((a).y)<<" )\n";
#define coud(a,b) cout<<fixed << setprecision((b)) << (a)
#define L(x) ((x)<<1)
#define R(x) (((x)<<1)+1)
#define umap unordered_map
#define double long double
typedef long long ll;
typedef pair<int,int>pii;
typedef vector<int> vi;
typedef complex<double> point;
template <typename T> using os =  tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <class T>  inline void smax(T &x,T y){ x = max((x), (y));}
template <class T>  inline void smin(T &x,T y){ x = min((x), (y));}
const int mod = 1e9 + 7;
inline int power(int a, ll b){
	int ans = 1;
	while(b){
		if(b & 1LL)	ans = (1LL * ans * a) % mod;
		a = (1LL * a * a) % mod;
		b >>= 1;
	}
	return ans;
}
int main(){
	int k;
	scanf("%d", &k);
	bool parity = 1, g = false;
	int x = 2;
	while(k--){
		ll a;
		scanf("%lld", &a);
		if(a > 1)	g = true;
		if(!(a & 1LL))	parity = 0;
		x = power(x, a);
	}
	if(!g){
		cout << "0/1\n";
		return 0;
	}
	parity = !parity;
	x = (1LL * x * power(2, mod-2)) % mod;
	int y = (mod + x + (parity? 1: -1)) % mod;
	y = (1LL * y * power(3, mod-2)) % mod;
	cout << y << "/" << x << endl;
	return 0;
}