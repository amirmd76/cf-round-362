#include <iostream>
using namespace std;
const int MOD = 1000000007;
const int MAX = 100005;
long long a[MAX];
int bpow(int a, int b)
{
	int ans = 1;
	while (b)
	{
		if (b & 1)
			ans = 1LL * ans * a % MOD;
		b >>= 1;
		a = 1LL * a * a % MOD;
	}
	return ans;
}
int main()
{
	ios::sync_with_stdio(false);
	cin.tie(0);
	int n;
	cin >> n;
	bool even = false;
	int mul = 1;
	for (int i = 0; i < n; i++)
	{
		cin >> a[i];
		if (a[i] % 2 == 0)
			even = true;
		mul = 1LL * mul * (a[i] % (MOD - 1)) % (MOD - 1);
	}
	mul = (mul - 1 + (MOD - 1)) % (MOD - 1);
	int num = bpow(2, mul);
	int den = num;
	if (even)
		num++;
	else
		num--;
	num = 1LL * num * bpow(3, MOD - 2) % MOD;
	cout << num << "/" << den << "\n";
	return 0;
}
