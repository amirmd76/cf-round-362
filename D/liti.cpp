//In the name of Allah
#include <bits/stdc++.h>
using namespace std;

typedef long long ll; 
const int maxN = 200 + 10; 

vector<string> str; 
string s[maxN]; 
int nex[maxN][26]; 
int v[maxN];
int w[maxN][26]; 

int a[maxN]; 

bool cmp( string& a , string& b ) { 
	if( a.size() == b.size() ) 
		return a < b; 
	return a.size() < b.size(); 
}

typedef ll Mat[maxN][maxN]; 
int N; 
Mat b,c,ans; 

void clear( Mat a ) { 
	for( int i = 0  ; i < N ; i++ ) 
		for( int j = 0 ; j < N ; j++ ) 
			a[i][j] = -1e18; 
}

void mul( Mat a , Mat b , Mat c ) { 
	clear(c); 
	for( int i = 0 ; i < N ; i++ ) 
		for( int j = 0 ; j < N ; j++ ) 
			for( int k = 0 ; k < N ; k++ ) 
				c[i][k] = max( c[i][k] , a[i][j] + b[j][k] ); 
}


int main() { 
	ios::sync_with_stdio(false); cin.tie(0); 
	int n; ll l; 
	cin >> n >> l;
	for( int i = 0 ; i < n ; i++ ) 
		cin >> a[i]; 

	str.push_back(""); 
	for( int i = 0 ; i < n ; i++ ) { 
		cin >> s[i]; 
		string tmp = ""; 
		for( auto ch : s[i] ) { 
			tmp += ch; 
			str.push_back(tmp); 
		}
	}
	sort( str.begin() , str.end() , cmp ) ; 
	str.resize( unique(str.begin(),str.end()) - str.begin() );

	N = str.size(); 
	for( int i = 0 ; i < n ; i++ ) 
		for( int j = 0 ; j < int(str.size()) ; j++ ) 
			if( str[j].size() >= s[i].size() && 
					str[j].substr( (int)str[j].size() - (int)s[i].size() , s[i].size() ) == s[i] ) 
				v[j] += a[i]; 

	for( int i = 0 ; i < int(str.size()) ; i++ ) { 
		string t = str[i]; 
		for( int j = 0 ;j < 26 ; j++ ) {
			char ch = 'a' + j; 
			t += ch; 
			for( int k = int(str.size()) - 1 ; k >= 0 ; k-- ) 
				if( str[k].size() <= t.size() && 
						t.substr( (int)t.size() - int(str[k].size()) , str[k].size() ) == str[k] ) { 
					nex[i][j] = k; 
					w[i][j] = v[k]; 
					break; 
				}
			t.pop_back(); 
		}
	}

	clear(ans); 
	clear(b); 
	for( int i = 0 ; i < N ; i++ ) 
		ans[i][i] = 0; 
	for( int i = 0 ; i < N ; i++ ) 
		for( int j = 0 ; j < 26 ; j++ ) 
			b[i][nex[i][j]] = w[i][j]; 

	for( ; l ; l /= 2 ) { 
		if( l % 2 ) { 
			mul( ans , b , c ); 
			swap( ans , c ); 
		}
		mul( b , b , c ); 
		swap( b , c ); 
	}
	ll mx = 0; 
	for( int i = 0 ; i < N ; i++ ) 
		mx = max( mx , ans[0][i] ) ; 
	cout << mx << endl;
}
