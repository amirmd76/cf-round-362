import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 *
 * @author AlexFetisov
 */
public class Main {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        TaskD_362 solver = new TaskD_362();
        solver.solve(1, in, out);
        out.close();
    }

    static class TaskD_362 {
        Node[] nodes;

        public void solve(int testNumber, InputReader in, PrintWriter out) {
            int n = in.nextInt();
            long L = in.nextLong();
            int[] a = Utils.readIntArray(in, n);
            int[][] words = new int[n][];
            for (int i = 0; i < n; ++i) {
                char[] c = in.nextString().toCharArray();
                words[i] = new int[c.length];
                for (int j = 0; j < c.length; ++j) {
                    words[i][j] = c[j] - 'a';
                }
            }
            int countNodes = buildAho(words, a);
            long[][] A = new long[countNodes][countNodes];
            ArrayUtils.fill(A, -1);
            for (int i = 0; i < countNodes; ++i) {
                for (int with = 0; with < 26; ++with) {
                    int to = nodes[i].next[with].id;
                    A[i][to] = Math.max(A[i][to], nodes[i].next[with].weight);
                }
            }
            A = pow(A, L);
            long res = 0;
            for (int i = 0; i < countNodes; ++i) {
                res = Math.max(res, A[0][i]);
            }
            out.println(res);
        }

        public static long[][] pow(long[][] a, long p) {
            int n = a.length;
            long[][] r = new long[n][n];
            ArrayUtils.fill(r, -1);
            for (int i = 0; i < n; ++i) r[i][i] = 0;
            for (; p > 0; p /= 2) {
                if ((p & 1) > 0) {
                    r = mul(r, a);
                }
                a = mul(a, a);
            }
            return r;
        }

        private static long[][] mul(long[][] a, long[][] b) {
            int n = a.length;
            long[][] r = new long[n][n];
            ArrayUtils.fill(r, -1);
            for (int i = 0; i < n; ++i) {
                for (int j = 0; j < n; ++j) {
                    for (int k = 0; k < n; ++k) {
                        if (a[i][k] != -1 && b[k][j] != -1) {
                            r[i][j] = Math.max(r[i][j], a[i][k] + b[k][j]);
                        }
                    }
                }
            }
            return r;
        }

        int buildAho(int[][] initData, int[] value) {
            nodes = new Node[213];
            nodes[0] = new Node();
            Node root = nodes[0];
            root.id = 0;
            int n = initData.length;
            int countNodes = 1;
            int free = 1;
            for (int i = 0; i < n; ++i) {
                Node cur = root;
                for (int it = 0; it < initData[i].length; ++it) {
                    int c = initData[i][it];
                    if (cur.next[c] == null) {
                        nodes[free] = new Node();
                        cur.next[c] = nodes[free];
                        nodes[free].id = free;
                        ++free;
                        countNodes++;
                    }
                    cur = cur.next[c];
                }
                cur.weight += value[i];
            }
            Node[] q = new Node[countNodes];
            q[0] = root;
            int e = 1;
            for (int t = 0; t < e; ++t) {
                Node node = q[t];
                for (int c = 0; c < 26; ++c) {
                    if (node.next[c] == null) {
                        node.next[c] = node == root ? root : node.link.next[c];
                    } else {
                        node.next[c].link = node == root ? root : node.link.next[c];
                        q[e++] = node.next[c];
                    }
                }
            }
            for (int i = 1; i < e; ++i) {
                q[i].weight += q[i].link.weight;
            }
            return countNodes;
        }

        public class Node {
            Node[] next = new Node[26];
            Node link;
            int id;
            int weight;

        }

    }

    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer stt;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                return null;
            }
        }

        public String nextString() {
            while (stt == null || !stt.hasMoreTokens()) {
                stt = new StringTokenizer(nextLine());
            }
            return stt.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(nextString());
        }

        public long nextLong() {
            return Long.parseLong(nextString());
        }

    }

    static class Utils {
        public static int[] readIntArray(InputReader in, int n) {
            int[] a = new int[n];
            for (int i = 0; i < n; ++i) {
                a[i] = in.nextInt();
            }
            return a;
        }

    }

    static class ArrayUtils {
        public static void fill(long[][] a, int value) {
            for (int i = 0; i < a.length; ++i) {
                Arrays.fill(a[i], value);
            }
        }

    }
}

