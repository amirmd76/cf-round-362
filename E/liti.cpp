//In the name of Allah
#include <bits/stdc++.h>
using namespace std;

typedef long long ll; 
typedef pair<ll,int> pii; 

const int maxN = 1000 * 100 + 10; 
const int maxL = 20; 
const ll inf = 1e15; 
const pii bad = { inf + 100 , maxN } ; 

vector<int> c[maxN]; 
vector<ll> val[maxN]; 

int par[maxN][maxL], h[maxN]; 
int sz[maxN], st[maxN], en[maxN]; 
int no[maxN]; 
int head[maxN]; 

pii mn[4*maxN]; 
ll ad[4*maxN]; 
int N; 

void build( int xl = 0 , int xr = N , int ind = 1 ) { 
	if( xr - xl == 1 ) 
		return void( mn[ind] = pii( val[no[xl]].back() , no[xl] )); 
	
	int xm = (xl+xr)/ 2; 
	build( xl , xm , ind * 2 ); 
	build( xm , xr , ind * 2 + 1 ); 
	mn[ind] = min( mn[ind*2] , mn[ind*2+1] ); 
}

void addVal( int ql , int qr , ll qv , int xl = 0 , int xr = N , int ind = 1 ) { 
	if( xr <= ql || qr <= xl ) return ; 
	if( ql <= xl && xr <= qr ) { 
		ad[ind] += qv; 
		mn[ind].first += qv; 
		return;
	}

	int xm = (xl+xr)/2; 
	addVal( ql , qr , qv , xl , xm , ind * 2 ); 
	addVal( ql , qr , qv , xm , xr , ind * 2 + 1 ) ; 
	mn[ind] = min( mn[ind*2] , mn[ind*2+1] ) ; 
	mn[ind].first += ad[ind]; 
}

pii getMin( int ql , int qr , int xl = 0 , int xr = N , int ind = 1 ) { 
	if( xr <= ql || qr <= xl ) return bad; 
	if( ql <= xl && xr <= qr ) return mn[ind]; 
	int xm = (xl+xr)/2; 
	pii ret = min( getMin( ql , qr , xl , xm , ind * 2 ) , 
			getMin( ql , qr , xm , xr , ind * 2 + 1 ) ) ; 
	ret.first += ad[ind]; 
	return ret; 
}

inline pii getPath( int u , int w ) { 
	pii ret = bad; 
	while( u != -1 && st[u] >= st[w] ) { 
		int v = head[u]; 
		ret = min( ret , getMin( max( st[v] , st[w] ) , st[u] + 1 ) ); 
		u = par[v][0]; 
	}
	return ret; 
}


int dsz( int s , int p = -1 ) { 
	for( auto x : c[s] ) 
		if( x != p ) 
			sz[s] += dsz(x,s); 
	return ++sz[s]; 
}

void dfs( int s , int head , int p) { 
	static int ind = 0 ; 
	st[s] = ind++; 
	no[st[s]] = s; 
	::head[s] = head; 
	par[s][0] = p; 
	for( int k = 1 ; k < maxL ; k++ ) 
		par[s][k] = par[par[s][k-1]][k-1]; 

	if( (int)c[s].size() - 1 + (s==0) ) { 
		int mx = c[s][0]; 
		if( mx == p ) mx = c[s][1]; 
		for( auto x : c[s] ) if( x != p && sz[mx] < sz[x] ) 
			mx = x; 
		h[mx] = h[s] + 1; 
		dfs( mx , head , s ); 
		for( auto x : c[s] ) if( x != p && x != mx ) { 
			h[x] = h[s] + 1; 
			dfs( x , x , s ); 
		}
	}

	en[s] = ind; 
}

inline int getH( int u , int h ) { 
	for( int i = 0 ; i < maxL ; i++ ) 
		if( (h>>i) & 1 ) 
			u = par[u][i]; 
	return u; 
}

inline int LCA( int u , int v ) { 
	if( h[u] > h[v] ) swap( u , v ); 
	v = getH( v , h[v] - h[u] ) ; 
	if( u == v ) return u; 
	for( int k = maxL - 1 ; k >= 0; k-- ) 
		if( par[u][k] != par[v][k] ) { 
			u = par[u][k]; 
			v = par[v][k]; 
		}
	return par[u][0]; 
}


ll tmp[maxN]; int tc;

int main() { 
	ios::sync_with_stdio(false); cin.tie(0); 

	int n,m,q; 
	cin >> n >> m >> q; 
	N = n; 

	for( int i = 0 ; i < n - 1 ; i++ ) { 
		int u,v; cin >> u >> v; 
		u--; v--; 
		c[u].push_back(v); 
		c[v].push_back(u); 
	}
	dsz(0); 
	dfs(0,0,0); 
	par[0][0] = -1;

	for( int i = 0 ; i < m ; i++ ) { 
		int x; cin >> x; x--; 
		val[x].push_back(i+1); 
	}
	for( int i = 0 ; i < n ; i++ ) { 
		val[i].push_back(inf); 
		sort( val[i].begin() , val[i].end() ) ; 
		reverse(val[i].begin(), val[i].end()) ; 
	}

	build(); 

	for( int i = 0 ; i < q ; i++ ) { 
		int t; cin >> t; 
		if( t == 1 ) { 
			int v,u,k; cin >> v >> u >> k; 
			v--; u--;
			int w = LCA(u,v); 
			tc = 0; 
			for( int j = 0 ; j < k ; j++ ) { 
				pii mn = min( getPath( u , w ) , getPath( v , w ) ); 
				if( mn.first >= inf ) break; 
				int x = mn.second; 
				tmp[tc++] = val[x].back(); 
				ll v = -val[x].back(); 
				val[x].pop_back(); 
				v += val[x].back(); 
				addVal( st[x] , st[x] + 1 , v ); 
			}
			cout << tc << ' ' ; 
			for( int j = 0 ; j < tc ; j++ ) 
				cout << tmp[j] << ' ' ; 
			cout << '\n'; 
		} else { 
			int v,k; cin >> v >> k; 
			v--; 
			addVal( st[v] , en[v] , k ) ; 
		}
	}
}
