#include <bits/stdc++.h>

#define forn(i,n) for (int i = 0; i < int(n); ++i)
#define nfor(i,n) for (int i = int(n) - 1; i >= 0; --i)
#define sz(a) int(a.size())
#define pb push_back
#define mp make_pair

using namespace std;

const int N = int(1e5) + 5;
const int LOGN = 20;
typedef long long li;

const li INF = 1e18;

int n, m, q;
vector <int> g[N];
int cnt[N];
int tin[N], tout[N], T;

bool used[N];
vector <int> girls[N];
int p[LOGN][N], c[N];
li a[N];

void dfs(int v, int pp) {
	p[0][v] = pp;
	forn(i, LOGN - 1)
		p[i + 1][v] = p[i][ p[i][v] ];

	tin[v] = T++;
	cnt[v] = 1;

	for (auto to: g[v]) {
		if (to == pp)
			continue;
		dfs(to, v);
		cnt[v] += cnt[to];
	}

	tout[v] = T;
}

bool is_ancestor(int u, int v) {
	return tin[u] <= tin[v] && tout[v] <= tout[u];
}

inline bool heavy(int v, int to) { return cnt[to] > cnt[v] / 2; }
int idx[N];

vector< vector<int> > paths;
int st[N], pos[N];
vector<int> vs;

int last[N];
li add[4 * N];
pair<li, int> bb[4 * N];

void push(int v, int l, int r) {
	if (add[v] != 0) {
		bb[v].first += add[v];
		if (l + 1 < r) {
			add[v * 2 + 1] += add[v];
			add[v * 2 + 2] += add[v];
		}
		add[v] = 0;
	}
}

inline pair<li, int> getbest(int v, int l, int r, int L, int R) {
	assert(L < R);

	push(v, l, r);
	if (l == L && r == R)
		return bb[v];

	int m = (l + r) >> 1;

	pair <li, int> res = mp(INF, -1);
	
	if (L < m)
		res = min(res, getbest(v * 2 + 1, l, m, L, min(m, R)));
	if (R > m)
		res = min(res, getbest(v * 2 + 2, m, r, max(L, m), R));
	return res;
}

void update_inc(int v, int l, int r, int L, int R, int delta) {
	if (L == R)
		return;

	push(v, l, r);

	if (L == l && R == r) {
		add[v] += delta;
		push(v, l, r);
		return;
	}
	
	int m = (l + r) >> 1;

	if (L < m)
		update_inc(v * 2 + 1, l, m, L, min(m, R), delta);
	if (R > m)
		update_inc(v * 2 + 2, m, r, max(L, m), R, delta);

	push(v * 2 + 1, l, m);
	push(v * 2 + 2, m, r);
	bb[v] = min(bb[v * 2 + 1], bb[v * 2 + 2]);
}

void update_pop(int v, int l, int r, int key) {
	push(v, l, r);
	if (l + 1 == r) {
		assert(l == key);

		int vv = vs[key];
		assert(sz(girls[vv]));
	
		bb[v].first -= girls[vv].back();
		girls[vv].pop_back();
		if (sz(girls[vv]))
			bb[v].first += girls[vv].back();
		else
			bb[v] = mp(INF, -1);
		return;
	}

	int m = (l + r) >> 1;

	if (key < m)
		update_pop(v * 2 + 1, l, m, key);
	else
		update_pop(v * 2 + 2, m, r, key);

	push(v * 2 + 1, l, m);
	push(v * 2 + 2, m, r);
	bb[v] = min(bb[v * 2 + 1], bb[v * 2 + 2]);
}

void build(int v, int l, int r) {
	add[v] = 0;
	if (l + 1 == r) {
		int vv = vs[l];
		if (girls[vv].empty())
			bb[v] = mp(INF, -1);
		else
			bb[v] = mp(li(girls[vv].back()), vv);
		return;
	}

	int m = (l + r) >> 1;
	build(v * 2 + 1, l, m);
	build(v * 2 + 2, m, r);

	bb[v] = min(bb[v * 2 + 1], bb[v * 2 + 2]);
}

void dfs2(int v, int p) {
	idx[v] = sz(paths);
	if (p != -1 && heavy(p, v)) idx[v] = idx[p];
	else paths.pb(vector<int> ());
	paths[idx[v]].pb(v);
	forn(i, sz(g[v])) if (g[v][i] != p) dfs2(g[v][i], v);
}

inline void prepareHL() {
	dfs(0, 0); // prepare lca, cnt
	paths.clear();
	vs.clear();
	dfs2(0, -1);
	forn(i, sz(paths)) {
		st[i] = sz(vs);
		forn(j, sz(paths[i])) {
			pos[paths[i][j]] = sz(vs);
			vs.pb(paths[i][j]);
		}
		last[i] = vs.back();
	}

	build(0, 0, sz(vs));
}

li ft[N];

inline void inc(int r, li delta) {
	for (; r < N; r |= (r + 1))
		ft[r] += delta;
}

inline li get(int r) {
	li res = 0;
	for (; r >= 0; r = (r & (r + 1)) - 1)
		res += ft[r];
	return res;
}

inline pair<li, int> calc(int v, int l) {
	for (pair <li, int> ans = mp(INF, -1); ; v = p[0][vs[st[idx[v]]]]) {
		auto cur = getbest(0, 0, sz(vs), max(pos[l], st[idx[v]]), pos[v] + 1);
		if (cur.second != -1)
			cur.first += get(tin[last[idx[v]]]);
		ans = min(ans, cur);
		if (idx[v] == idx[l]) return ans;
	}
}

inline int lca(int v, int u) {
	if (is_ancestor(v, u))
		return v;
	if (is_ancestor(u, v))
		return u;
	nfor(i, LOGN)
		if (!is_ancestor(p[i][u], v))
			u = p[i][u];
	assert(!is_ancestor(u, v));
	u = p[0][u];
	assert(is_ancestor(u, v));
	return u;
}

inline pair<li, int> calc_path(int u, int v) {
	int l = lca(u, v);
	return min(calc(u, l), calc(v, l));
}

int main() {
	assert(cin >> n >> m >> q);

	forn(i, n - 1) {
		int u, v;
		assert(scanf("%d%d", &u, &v) == 2);
		--u, --v;
		g[u].push_back(v);
		g[v].push_back(u);
	}
	
	forn(i, m) {
		assert(scanf("%d", &c[i]) == 1);
		c[i]--;
	}

	nfor(i, m)
		girls[c[i]].push_back(i);

	memset(tin, -1, sizeof(tin));
	memset(tout, -1, sizeof(tout));

	prepareHL();

	forn(_, q) {
		int t;
		assert(scanf("%d", &t) == 1);
		if (t == 1) {
			int u, v, k;
			assert(scanf("%d%d%d", &u, &v, &k) == 3);
			--u, --v;

			vector <int> gg;
			vector <li> ws;
			forn(_, k) {
				auto p = calc_path(u, v);
				if (p.second == -1)
					break;
				gg.pb(girls[p.second].back());
				ws.pb(p.first);
				update_pop(0, 0, sz(vs), pos[p.second]);
			}

			printf("%d", sz(gg));
			for (auto x: gg)
				printf(" %lld", li(x + 1));
			puts("");
		} else {
			int v, k;
			assert(scanf("%d%d", &v, &k) == 2);
			--v;

			inc(tin[v], +k);
			inc(tout[v], -k);

			update_inc(0, 0, sz(vs), st[idx[v]], pos[v], -k);
		}
	}

	return 0;
}
